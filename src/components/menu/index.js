import React from "react"
import { Link } from "gatsby"
import styled from "styled-components"

// Utils
import breakpoint from "utils/breakpoints/"

// Components
import Container from "components/container/"

// Icons
import Isologo from "assets/icons/isologo.inline.svg"

const StyledMenu = styled.nav`
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  padding: 32px 0;

  ${breakpoint.small`
    padding: 90px 0;
  `}

  ${Container} {
    display: flex;
    align-items: center;
  }

  .menu__logo {
    display: flex;

    svg {
      width: 48px;
      height: auto;

      ${breakpoint.small`
        width: 72px;
      `}
    }
  }

  ul {
    margin-left: 32px;

    li {
      a {
        font-size: 14px;
        font-weight: 700;
        line-height: 1.3em;
        text-transform: uppercase;
        letter-spacing: 0.02em;

        ${breakpoint.small`
          font-size: 18px;
        `}
      }
    }
  }
`

const Menu = () => (
  <StyledMenu>
    <Container>
      <Link className="menu__logo" to="/">
        <Isologo />
      </Link>

      <ul>
        <li>
          <Link to="/work">Work</Link>
        </li>
      </ul>
    </Container>
  </StyledMenu>
)

export default Menu
