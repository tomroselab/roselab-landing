import React from "react"
import { StaticQuery, graphql } from "gatsby"
import styled from "styled-components"

// Utils
import breakpoint from "utils/breakpoints/"

// Components
import Container from "components/container/"
import Grid from "components/grid/"

const StyledDribbble = styled.section`
  padding-top: 72px;

  ${breakpoint.small`
    padding-top: 140px;
  `}
`

const DribbbleItem = styled.div`
  width: 100%;
  padding-bottom: 75%;
  background-color: #e6e3c7;
  background-image: ${props =>
    props.backgroundImage ? `url('${props.backgroundImage}')` : ""};
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  transition: all 0.3s ease;

  &:hover {
    transform: scale(1.05);
  }
`

const Dribbble = () => (
  <StyledDribbble>
    <Container>
      <h3 className="mb-3 mb-md-5">Dribbble Shots</h3>
      <Grid columns="3" gutter="32">
        <StaticQuery
          query={graphql`
            query dribbbleShotsQuery {
              allDribbbleShot(
                sort: { fields: [published], order: DESC }
                limit: 9
              ) {
                edges {
                  node {
                    url
                    cover
                  }
                }
              }
            }
          `}
          render={data =>
            data.allDribbbleShot.edges.map(shot => (
              <div className="grid__item" key={shot.node.url}>
                <a
                  href={shot.node.url}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <DribbbleItem backgroundImage={shot.node.cover} />
                </a>
              </div>
            ))
          }
        />
      </Grid>
    </Container>
  </StyledDribbble>
)

export default Dribbble
