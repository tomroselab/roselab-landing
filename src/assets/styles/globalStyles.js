// Styles
import "reset-css/reset.css"
import "./bootstrap.scss"

// Helpers
import { Helpers } from "./helpers"

// Libraries
import { createGlobalStyle } from "styled-components"

// Utils
import { colors } from "utils/variables/"
import breakpoint from "utils/breakpoints/"

// Fonts
import SangBleuEmpireBlack from "assets/fonts/SangBleuEmpireBlack.otf"
import SctoGroteskMedium from "assets/fonts/SctoGroteskMedium.otf"

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'SangBleu Empire';
    src: url('${SangBleuEmpireBlack}');
    font-weight: 900;
    font-style: normal;
  }

  @font-face {
    font-family: 'Scto Grotesk';
    src: url('${SctoGroteskMedium}');
    font-weight: 500;
    font-style: normal;
  }

  body {
    background-color: ${colors.cream};
    color: ${colors.blue};
    font-family: 'Scto Grotesk';
    font-weight: 500;
    font-size: 20px;
    line-height: 1.6em;


    div,
    header,
    section {
      box-sizing: border-box;
    }

    h1,
    h2 {
      font-family: 'SangBleu Empire';
      font-weight: 900;
      line-height: 1.35em;
    }

    h1 {
      font-size: 44px;

      ${breakpoint.medium`
        font-size: 56px;
      `}
    }

    h2 {
      font-size: 28px;

      ${breakpoint.medium`
        font-size: 40px;
      `}
    }

    h3 {
      font-size: 24px;
      font-weight: 400;

      ${breakpoint.medium`
        font-size: 32px;
      `}
    }

    p {

      &.paragraph--subtitle {
        font-size: 20px;
        line-height: 28px;

        ${breakpoint.medium`
          font-size: 24px;
          line-height: 32px;
        `}
      }
    }

    a {
      color: inherit;
      text-decoration: none;
      transition: all .3s;
    }
  }

  ${Helpers}
`

export default GlobalStyle
