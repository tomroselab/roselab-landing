import React from 'react'
import Helmet from 'react-helmet'

const SEO = () => (
  <Helmet title="Roselab">
    <meta name="description" content="We grow user-centered culture in companies by developing and designing astonishing digital experience" />
  </Helmet>
)

export default SEO