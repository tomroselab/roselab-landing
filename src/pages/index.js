import React from "react"

// Layout
import Layout from "layouts/layout-primary"

// Page Sections
import Hero from "sections/homepage/Hero"
import Banner from "sections/homepage/Banner"
import About from "sections/homepage/About"

const Homepage = () => (
  <Layout>
    <Hero />
    <Banner />
    <About />
  </Layout>
)

export default Homepage
