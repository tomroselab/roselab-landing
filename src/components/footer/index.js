import React from "react"
import styled from "styled-components"

// Utils
import breakpoint from "utils/breakpoints/"
import { colors } from "utils/variables/"

// Components
import Container from "components/container/"
import MobileIllustration from "assets/icons/mail-mobile-illustration.inline.svg"
import DesktopIllustration from "assets/icons/mail-desktop-illustration.inline.svg"
import SendIcon from "assets/icons/send-icon.inline.svg"

const StyledFooter = styled.footer`
  padding-top: 48px;
  background-color: ${colors.cream};

  ${breakpoint.small`
    padding-top: 88px;
  `}

  .email {
    display: inline-flex;
    align-items: center;
    position: relative;
    margin-bottom: 88px;
    font-size: 32px;
    line-height: 1.3em;

    &:after {
      content: "";
      width: 35%;
      height: 2px;
      position: absolute;
      bottom: -12px;
      background-color: ${colors.blue};
      transition: all 0.3s;
    }

    &:hover {
      color: ${colors.red};

      &:after {
        width: 100%;
        background-color: ${colors.red};
      }

      svg {
        * {
          stroke: ${colors.red};
        }
      }
    }

    ${breakpoint.small`
      font-size: 40px;
      margin-bottom: 0;
      bottom: -64px;
    `}

    ${breakpoint.medium`
      bottom: -96px;
      font-size: 48px;
    `}
    
    svg {
      margin-left: 16px;

      * {
        transition: all 0.3s;
      }
    }
  }
`

const StyledMobileIllustration = styled(MobileIllustration)`
  width: 100%;

  ${breakpoint.small`
    display: none;
  `}
`

const StyledDesktopIllustration = styled(DesktopIllustration)`
  display: none;

  ${breakpoint.small`
    max-width: 100%;
    height: auto;
    display: flex;
  `}
`

const Footer = () => (
  <StyledFooter>
    <Container>
      <a
        className="email"
        href="mailto:info@roselab.co"
        target="noreferrer noopener"
      >
        info@roselab.co
        <SendIcon />
      </a>
      <StyledDesktopIllustration />
    </Container>
    <StyledMobileIllustration />
  </StyledFooter>
)

export default Footer
