import React from "react"

// Layout
import Layout from "layouts/layout-primary"

// Sections
import Hero from "sections/work/hero"
import Websites from "sections/work/websites"
import Dribbble from "sections/work/dribbble"

const Work = () => (
  <Layout>
    <Hero />
    <Websites />
    <Dribbble />
  </Layout>
)

export default Work
