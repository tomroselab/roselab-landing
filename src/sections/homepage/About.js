import React from "react"
import styled from "styled-components"
import { colors } from "utils/variables/"
import breakpoint from "utils/breakpoints/"

// Components imports
import Container from "components/container/"
import Grid from "components/grid/"
import DesignLogo from "assets/icons/roselab-design-logo.inline.svg"
import DevelopmentLogo from "assets/icons/roselab-development-logo.inline.svg"
import ConsultancyLogo from "assets/icons/roselab-consultancy-logo.inline.svg"

const StyledAbout = styled.section`
  width: 100%;
  padding-top: 64px;
  background-color: ${colors.cream};

  ${breakpoint.medium`
    padding-top: 96px;
  `}

  h1 {
    max-width: 562px;
    margin-bottom: 64px;
    color: ${colors.red};

    ${breakpoint.medium`
      max-width: 736px;
      margin-bottom: 88px;
    `}
  }
`

const Branch = styled.div`
  display: flex;
  flex-wrap: wrap;

  ${breakpoint.small`
    flex-wrap: nowrap;
  `}

  ${breakpoint.medium`
    flex-wrap: wrap;
  `}

  .branch__name {
    display: flex;
    margin-bottom: 24px;

    ${breakpoint.small`
      margin-bottom: 40px;
    `}

    svg {
      display: block; // Safari fallback
    }
  }

  .branch__info {
    .question {
      display: inline-block;
      padding: 4px;
      background-color: ${colors.blue};
      color: ${colors.cream};
      font-size: 18px;
      line-height: 1em;
      text-transform: uppercase;
    }

    ${breakpoint.small`
      width: calc(100% - 212px);
    `}

    ${breakpoint.medium`
      width: 100%;
    `}
  }
`

const About = () => (
  <StyledAbout>
    <Container>
      <Grid gutter="48" columns="3">
        <div className="grid__item">
          <Branch>
            <div className="branch__name">
              <DesignLogo />
            </div>
            <div className="branch__info">
              <p className="question">Why</p>
              <p>
                We emphasis in designing for all life, not just human life. With
                a life centered mindset we design for the entire planet helping
                develop an inclusive and sustainable society.
              </p>
              <br />
              <p className="question">What</p>
              <p>
                From branding and astonished interfaces to ideation workshops
                and design sprints, we always put people first, launching
                experiences that redefine business{" "}
              </p>
            </div>
          </Branch>
        </div>
        <div className="grid__item">
          <Branch>
            <div className="branch__name">
              <ConsultancyLogo />
            </div>
            <div className="branch__info">
              <p className="question">Why</p>
              <p>
                We are fans of analyzing and understanding human consuming
                behaviour, and we want to do it along with you while we engineer
                a business model for you idea.
              </p>
              <br />
              <p className="question">What</p>
              <p>
                We join you in the quest of getting to know your user and
                building a business around it
              </p>
            </div>
          </Branch>
        </div>
        <div className="grid__item">
          <Branch>
            <div className="branch__name">
              <DevelopmentLogo />
            </div>
            <div className="branch__info">
              <p className="question">Why</p>
              <p>
                We believe in developing affordable, accessible, top-of-the-edge
                websites that ensures the best experience for the users.
              </p>
              <br />
              <br />
              <p className="question">What</p>
              <p>
                We apply the latest technologies and put the user in the center
                of the experience, to create fast, fully functional and
                responsive websites that will help your business to be reached
                everywhere in the world.{" "}
              </p>
            </div>
          </Branch>
        </div>
      </Grid>
    </Container>
  </StyledAbout>
)

export default About
