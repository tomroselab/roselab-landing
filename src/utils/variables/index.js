// Colors
export const colors = {
  cream: "#FFFDEB",
  blue: "#15293F",
  red: "#E26060",
}

// Breakpoints
export const breakpoints = {
  small: "768",
  medium: "1024",
  large: "1200",
  extraLarge: "1440",
  wide: "1800",
}

// Font Weights:start
export const weights = {
  400: "400",
  500: "500",
  600: "600",
  700: "700",
}
// Font Weights:end
