import React from "react"

// Libraries
import styled from "styled-components"

// Utils
import breakpoint from "utils/breakpoints/"

// Components
import Container from "components/container/"
import Website from "components/work-website/"

const StyledWebsites = styled.section`
  padding-top: 70px;

  ${breakpoint.medium`
    padding-top: 140px;
  `}
`

const Websites = () => {
  const websites = [
    {
      title: "Margot",
      description:
        "A website to get Margot's essence across: Being environmentally friendly and cutting-edge is possible for all of us.",
      url: "http://margot-ec.com.ar",
    },
    {
      title: "Stanich",
      description:
        "We joined Stanich’s 10 years anniversary celebration helping this amazing company redesigning its website.",
      url: "https://www.stanichaudio.com.ar/",
    },
  ]

  return (
    <StyledWebsites>
      <Container>
        <h3 className="mb-3 mb-md-5 font-weight--700">Websites we built</h3>

        <div className="websites row">
          {websites.map((website, index) => (
            <div className="col-sm-6 mb-4 mb-sm-0" key={index}>
              <Website {...website} />
            </div>
          ))}
        </div>
      </Container>
    </StyledWebsites>
  )
}

export default Websites
