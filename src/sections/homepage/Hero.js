import React from "react"
import styled from "styled-components"
import { colors } from "utils/variables/"
import breakpoint from "utils/breakpoints/"

import Container from "components/container/"
import HeroBackground from "assets/images/homepage-hero-background.svg"

const StyledHero = styled.header`
  min-height: 100vh;
  display: flex;
  align-items: center;
  padding: 155px 0 32px 0;
  background-color: ${colors.cream};

  ${breakpoint.medium`
    background-image: url('${HeroBackground}');
    background-size: auto 100%;
    background-position: 110% bottom;
    background-repeat: no-repeat;
  `}

  svg {
    display: block;
    margin-bottom: 24px;
  }

  h1 {
    max-width: 727px;
    margin-bottom: 24px;
  }

  p {
    max-width: 434px;
    color: ${colors.red};

    ${breakpoint.medium`
      font-size: 24px;
    `}
  }
`

const Hero = () => (
  <StyledHero>
    <Container>
      <h1 data-aos="fade-in" data-aos-delay="250">
        Strategic design made to achieve business goals.
      </h1>
      <p data-aos="fade-in" data-aos-delay="250">
        Roselab is a remote team offering digital solutions by combining design,
        consultancy and development.
      </p>
    </Container>
  </StyledHero>
)

export default Hero
