import styled from "styled-components"
import breakpoint from "utils/breakpoints/"

const Container = styled.div`
  max-width: 1140px;
  width: 100%;
  padding: 0 24px;
  margin: 0 auto;

  ${breakpoint.small`
    padding: 0 48px;
  `}

  ${breakpoint.large`
    padding: 0;
  `}
`

export default Container
