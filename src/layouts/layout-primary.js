import React from "react"
import styled from "styled-components"

// Utils
import { colors } from "utils/variables/"

// Styles
import GlobalStyles from "assets/styles/globalStyles.js"

// SEO
import SEO from "components/seo/"

// Components
import Menu from "components/menu/"
import Footer from "components/footer/"

const StyledLayout = styled.main`
  background-color: ${colors.cream};
`

const Layout = props => (
  <StyledLayout>
    <SEO />
    <GlobalStyles />
    <Menu />
    {props.children}
    <Footer />
  </StyledLayout>
)

export default Layout
