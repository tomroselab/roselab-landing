import React from "react"

// Libraries
import styled from "styled-components"

// Utils
import { colors } from "utils/variables/"

// Icons
import IconExternalLink from "assets/icons/icon-external-link.inline.svg"

const StyledWebsite = styled.a`
  display: block;
  padding-left: 22px;

  &:hover {
    .link {
      color: ${colors.blue};
      border-color: ${colors.blue};

      svg {
        * {
          stroke: ${colors.blue};
        }
      }
    }
  }

  h3 {
    position: relative;

    &::before {
      content: "";
      width: 1px;
      height: 100%;
      position: absolute;
      left: -22px;
      background-color: ${colors.red};
    }
  }

  .link {
    border-bottom: 1px solid ${colors.red};
    transition: all 0.3s ease;

    svg {
      margin-left: 8px;
      transition: all 0.3s ease;
    }
  }
`

const Website = props => {
  const { title, description, url } = props

  return (
    <StyledWebsite href={url} target="_blank" rel="noopener noreferrer">
      <h3 className="mb-3 font-weight--400">{title}</h3>
      <p className="mb-4 font-weight--400">{description}</p>
      <p className="link d-inline-flex align-items-center color--red">
        Open website
        <IconExternalLink />
      </p>
    </StyledWebsite>
  )
}

export default Website
