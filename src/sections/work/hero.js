import React from "react"

// Libraries
import styled from "styled-components"

// Components
import Container from "components/container/"

const StyledHero = styled.section`
  width: 100%;
  height: 100vh;
`

const Hero = () => {
  return (
    <StyledHero className="d-flex align-items-center justify-content-center bg--blue">
      <Container>
        <div className="col-sm-6">
          <h1 className="color--cream mb-4">A place of doers</h1>
          <p className="paragraph--subtitle color--red">
            We have the skills to imagine things that doesn’t exist yet,
            enabling us to take action and create meaningful new stuff.
          </p>
        </div>
      </Container>
    </StyledHero>
  )
}

export default Hero
