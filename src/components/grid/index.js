import styled from "styled-components"
import breakpoint from "utils/breakpoints/"

const Grid = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;

  .grid__item {
    width: 100%;
    margin-bottom: ${props => (props.gutter ? `${props.gutter}px` : "32px")};

    &:last-child {
      margin-bottom: 0;
    }

    ${breakpoint.small`
      width: 
        ${
          props =>
            props.columns < 3
              ? `calc((100% - (32px * (${props.columns} - 1))) / ${props.columns})`
              : `100%` // One column if there are more than 3 columns on this breakpoint
        };
    `}

    ${breakpoint.medium`
      width: ${props =>
        props.columns
          ? `calc((100% - (32px * (${props.columns} - 1))) / ${props.columns})`
          : "100%"};

      &:nth-last-child(-n + ${props => (props.columns ? props.columns : "1")}) {
        margin-bottom: 0;
      }
    `}
  }
`

export default Grid
