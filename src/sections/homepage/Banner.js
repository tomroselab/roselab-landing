import React from "react"
import styled from "styled-components"

// Utils
import { colors } from "utils/variables/"
import breakpoint from "utils/breakpoints/"

// Components
import Container from "components/container/"

const StyledBanner = styled.section`
  padding: 44px 0;
  background-color: ${colors.blue};
  color: ${colors.cream};

  ${breakpoint.small`
    padding: 88px 0;
  `}

  h1 {
    max-width: 736px;
    margin: 0 auto;
    text-align: center;
  }
`

const Banner = () => (
  <StyledBanner>
    <Container data-aos="fade">
      <h1>
        We simplify business growth by user-centering our clients’ culture.
      </h1>
    </Container>
  </StyledBanner>
)

export default Banner
